#include <iostream>
#include <string>
#include <sstream>
using namespace std;

// OpenCV includes
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
using namespace cv;

int main( int argc, const char** argv )
{
	// Read images
	Mat lena= imread("../lena.jpg");
	Mat photo= imread("../photo.jpg");
	
	// Create windows
	namedWindow("Lena", CV_GUI_NORMAL);
	namedWindow("Photo", CV_WINDOW_NORMAL);

	// Move window
	moveWindow("Lena", 10, 10);
	moveWindow("Photo", 530, 10);
	
	// show images
	imshow("Lena", lena);
	imshow("Photo", photo);

	// Resize window
	resizeWindow("Photo", 100, 100);

	// wait for any key press
	waitKey(0);
	return 0;
}